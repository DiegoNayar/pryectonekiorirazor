﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoNekioriCoreRazor.Models
{
    public class Disco
    {
        public int ID { get; set; }
        public string Marca { get; set; }
        public string Modelo { get; set; }
        public string Capacidad { get; set; }
        public string Serial { get; set; }
        public string TipoDispositivo { get; set; }
        public string Interfaz { get; set; }
        public int? IdCaso { get; set; }

        public virtual Caso IdCasoNavigation { get; set; }
    }
}
    
