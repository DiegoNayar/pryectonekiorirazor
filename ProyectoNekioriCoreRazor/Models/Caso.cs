﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoNekioriCoreRazor.Models
{
    public class Caso
    {
        public Caso()
        {
            Disco = new HashSet<Disco>();
        }

        public int ID { get; set; }
        public int? NroCaso { get; set; }
        public string Estado { get; set; }
        public string CantDatos { get; set; }
        public DateTime FechaIngreso { get; set; }
        public string Listado { get; set; }
        public int? IdCliente { get; set; }

        public virtual Cliente IdClienteNavigation { get; set; }
        public virtual ICollection<Disco> Disco { get; set; }
    }
}
