﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProyectoNekioriCoreRazor.Models
{
    public class Cliente
    {
        public Cliente()
        {
            Caso = new HashSet<Caso>();
        }

        public int ID { get; set; }
        public string Empresa { get; set; }
        public string Direccion { get; set; }
        public string Ciudad { get; set; }
        public string Comuna { get; set; }
        public string Contacto { get; set; }
        public string Telefono1 { get; set; }
        public string Telefono2 { get; set; }
        public string Correo { get; set; }

        public virtual ICollection<Caso> Caso { get; set; }
    }
}
